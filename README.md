Implementation of python in docker images 
to use those implementations just copy the following links 

| Implemetation  | python version   |Category     |Docker Image                      | 
|:---------------|:----------------:|:-----------:|---------------------------------:|
|Cpython2        | Python 2.7       | Interpreter | python:2.7                       |
|Cpython3        | Python 3.6       | Interpreter | python:3.6                       |
|PyPy2           | Python 2.7       | Interpreter | chakibmed/pypy2:original         |
|PyPy3           | Python 3.5       | Interpreter | chakibmed/pypy3:original         |
|Pyston          | Python 2.7       | Interpreter | pyston/pyston                    |
|ActivePython    | Python 3.6       | Transpiler  | chakibmed/activepython:original  |
|Cython          | Python 2.7       | Transpiler  | chakibmed/cython:original        |
|Shedskin        | Python 2.7       | Transpiler  | chakibmed/shedskin:original      |
|Nuitka2         | Python 2.7       | Transpiler  | chakibmed/nuitka2:original       |
|Nuitka3         | Python 3.5       | Transpiler  | chakibmed/nuitka3:original       |
|Hope            | Python 2.7       | Library     | chakibmed/hope:original          |
|Numba2          | Python 2.7       | Library     | chakibmed/numba2:original        |
|Numba3          | Python 3.6       | Library     | chakibmed/numba3:original        |
